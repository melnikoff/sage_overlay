# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI=5

PYTHON_COMPAT=( python2_7 python3_3 python3_4 )
inherit eutils python-r1
DESCRIPTION="High-speed desktop pushing for SAGE"
HOMEPAGE="http://www.sagecommons.org/"
SLOT=0
SRC_URI="http://sage.melnikoff.org/packages/${PN}-${PV}.tar.bz2"
DEPEND="sage-base/sagelib"
KEYWORDS="amd64 x86"
LICENSE="Sage-3.0"
PYTHON_DEPEND="2"

src_compile() {
	qmake
	emake
}

src_install() {
	emake PREFIX="${D}" install
}
